
/* ****************************************************************************************************************** */

/* Chuong trinh hien thi thoi gian thuc su dung module RTC + EEPROM ds1307 va module TM1637 su dung giao thuc I2C */

#include <mbed.h>
#include <TM1637.h>
#include <ds1307.h>
#define CLK PB_6             /* Định nghĩa chân clock */
#define DIO PB_7             /* Định nghịa chân data */

/*      1
     +-----+
     |     |
   32|     |2
     |     |
     + 64  +
      +---+
     +     +
     |     |
   16|     |4
     |     |
     +-----+
        8
*/
uint16_t digits[] = {63, 6, 91, 79, 102, 109, 125, 7, 127, 111, 128};
   /* hiển thị led : 0  1   2   3   4   5     6   7   8    9    :           */
   /* ví dụ : 63 = 1 + 2 + 4 + 8+ 16 + 32 => hiển thị số 0 trên led 7 thanh */

TM1637::DisplayData_t all_on  = {0xFF, 0xFF, 0xFF, 0xFF};        /* Tạo biến có kiểu data DisplayData_t bên ngoài class TM1637 */
TM1637::DisplayData_t all_off = {0x00, 0x00, 0x00, 0x00};        /* Tạo biến có kiểu data DisplayData_t bên ngoài class TM1637 */
TM1637 CATALEX(CLK, DIO);

DS1307 RTC_clock(PB_11, PB_10);

typedef struct 
{
    int secs;                                                   /*   giay                 */
    int mins;                                                   /*   phut                 */     
    int hours;                                                  /*   gio                  */      
    int days;                                                   /*   ngay trong tuan      */    
    int dates;                                                  /*   ngay trong thang     */ 
    int months;                                                 /*   thang                */
    int years;                                                  /*   nam                  */
} time_info_t;
 
int main()
{
    time_info_t time = {0, 39, 11, 0, 0, 0, 0};

    RTC_clock.start_clock();
    RTC_clock.twentyfour_hour();
    RTC_clock.gettime(&time.secs, &time.mins, &time.hours, &time.days, &time.dates, &time.months, &time.years);
    RTC_clock.settime (time.secs, time.mins, time.hours, time.days, time.dates, time.months, time.years);
    
    CATALEX.cls();                                        /* clearscreen                       */
    CATALEX.writeData(all_off);                           /* ghi data vào object CATALEX       */
    CATALEX.setBrightness(TM1637_BRT7);                   /* thiết lập độ sáng của led 7 thanh */

    while (1)
    {
        RTC_clock.gettime (&time.secs, &time.mins, &time.hours, &time.days, &time.dates, &time.months, &time.years);
        TM1637::DisplayData_t dat = {digits[(int) (time.hours / 10)],digits[(int) (time.hours % 10)],\
        digits[(int) (time.mins / 10)],digits[(int) (time.mins % 10)]};
        
        TM1637::DisplayData_t dat1 = {digits[(int) (time.hours / 10)], digits[10] + digits[(int) (time.hours % 10)],\
        digits[(int) (time.mins / 10)],digits[(int) (time.mins % 10)]};

        CATALEX.writeData(dat1);
        wait(0.55);
        CATALEX.writeData(dat);
        wait(0.55);
    }
}
